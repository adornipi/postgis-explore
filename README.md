Passion Postgres + PostGis
==========================

* Clone the repo
* Run `make init`

* Download the ban address database as a CSV
    ```bash
    mkdir -p data
    wget -O data/adresses-france.csv.gz https://adresse.data.gouv.fr/data/ban/adresses/latest/csv/adresses-france.csv.gz
    ```

* Download the french municipalities contours:

    ```bash
    wget -O data/commune-frmetdrom.zip https://static.data.gouv.fr/resources/contours-communes-france-administrative-format-admin-express-avec-arrondissements/20220414-153330/commune-frmetdrom.zip
    ```

* Start the Postgis Database: `docker compose up -d`
* Run the `__main__.py` script with: `poetry run python .`

# Troubleshooting

To open a `psql` cli on the database, you can run:
```bash
docker exec -it -u postgres postgis_demo psql
```
